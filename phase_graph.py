#
# This module provides a support class to write plots of phase and phase setpoint
#
# It has a constructor that accept a FigureCanvasQTAggBase  and has four methods:
# 1 - to write phase setpoints
# 2 - to write phase trace data
# 3 - to update results
# 4 - to clear results


from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QSizePolicy
# base graph class
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas

import matplotlib
import matplotlib.figure

import numpy as np

from math import exp

GRAPH_SIZE = (640,480) # graph size in pixels
GRAPH_NAME = 'phase_graph'
SETPOINT_GRAPH_END_INI = 1.3E-3 #seconds. When the setpoint graph ends 
END_KNEE_RATIO = 1.1
PHASE_GRAPH_RATIO = 1.2
SETPOINT_GRAPH_POINTS = 1000
LEGEND_POS = 'upper right'

Y_FIXED_DIM = 5.0

class PhaseGraph(FigureCanvas):
    '''
        This class provide the graph widget to print phase graphs
    '''
    def __init__(self):
        '''
            Initialize the graph widget
        '''
        figure = matplotlib.figure.Figure()
        FigureCanvas.__init__(self, figure)
        self.setSizePolicy(QSizePolicy.Expanding,
                           QSizePolicy.Expanding)
        self.setMinimumSize(QtCore.QSize(GRAPH_SIZE[0], 
                                GRAPH_SIZE[1]))
        self.setObjectName(GRAPH_NAME)

        # set initial graph size
        self.graph_end = SETPOINT_GRAPH_END_INI
        self.max_y_value = +180.0
        self.min_y_value = -180.0

    def clear(self):
        '''
            Clear graph
        '''
        self.figure.clf()
        self.plot = self.figure.add_subplot(111)
        self.max_y_value = -180.0
        self.min_y_value = +180.0

    def add_setpoint_graph(self, amp, tau, delay, knee, phase, key_name):
        '''
            add a setpoint graph
            tau and amp are the time constant and amplitude of the phase setpoint
            delay is the delay time before setpoint takes place
            knee is the time when the knee (exp -> 0 ) happens
            phase is the phase of of the flattop
            key name is the name of the curve
        '''

        self.max_y_value = max([self.max_y_value, -amp + phase])
        self.min_y_value = min([self.min_y_value, phase])

        self.graph_end = knee * END_KNEE_RATIO

        def phasefun(t):
            '''
                this function produces the setpoint curve
            '''
            if (t >= delay) and (t < knee): # .. if in exponential region
                return (phase +
                       -amp * (np.exp(-t/tau)-exp(-knee/tau))/
                             (exp(-delay/tau) - exp(-knee/tau) ))
            else: # .. return phase at flattop
                return phase

        # produces the points
        setpoint_data = [(float(t)*1.0E6*self.graph_end/SETPOINT_GRAPH_POINTS, 
                        phasefun(float(t)*self.graph_end/SETPOINT_GRAPH_POINTS))
                        for t in range(SETPOINT_GRAPH_POINTS)]

        self.plot.plot(*zip(*setpoint_data), label = key_name)

    def add_phase_graph(self, curve, key_name):
        '''
            add phase graph as an x_y curve
            first elements of curve x
            second elements of curve y
            key name is the name of curve
        '''
        to_plot = [(el[0] * 1.0E6, el[1]) for el in zip(*curve)]
        self.plot.plot(*zip(*to_plot), label= key_name)
        self.max_y_value = max([max(curve[1]), self.max_y_value])
        self.min_y_value = min([min(curve[1]), self.min_y_value])

    def draw_line(self, position):
            '''
                draw a vertical red line on position
            '''
            self.plot.plot((position * 1.0E6, position * 1.0E6), (-180,180),
                            'r-')

    def display_graphs(self):
        '''
             display graphs
        '''
        #set legend
        self.plot.legend(loc = LEGEND_POS)
        #normalize axes
        self.plot.set_xlim([0.0, self.graph_end * 1.0E6])
        #self.plot.set_ylim([SETPOINT_PHASE_MIN, SETPOINT_PHASE_MAX])
        self.plot.set_ylim([self.min_y_value - Y_FIXED_DIM, self.max_y_value + Y_FIXED_DIM])
        self.plot.set_xlabel('Time ($\mu s$)')
        self.plot.set_ylabel('Phase (deg)')
        self.updateGeometry()
        self.draw()
