
#
# This module provides the main window of the Phase Optimizer Program
# It shows the phase and setpoint of a selected station. number of optimization step
# It can select-deselect Learning feed forward
# fit delay and tau-phase max-minimum phases can be set.
# It has an 'exit' button to stop the program
# It has a 'refresh' button to refresh the graph
# It has an 'optimization' button to perform optimization

from PyQt5 import QtGui, QtCore, uic
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QMainWindow, QDialog

# these modules are needed to plot matplotlib graph in QT4
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

# dialog that presents optimization results
import result_dialog
from result_dialog import ResultDialog

# dialog that asks to apply right settings for the optimization
import settings_dialog
from settings_dialog import SettingsDialog

# dialog that presents why the optimization failed
import error_dialog
from error_dialog import ErrorDialog

import phase_graph
from phase_graph import PhaseGraph

# doocs interface
from doocs_IO import DoocsIO

# shows a progress bar
from progress_dialog import ProgressDialog

# optimizer object
import optimizer
from optimizer import Optimizer

START_TIME_MARGIN = 40 #microseconds
STOP_TIME_MARGIN = 163 # microseconds


TIME_DELAY = 0.1

MAIN_GUI_FILE = 'main_gui.ui' # the file where the main gui of the program resides 
STATIONS = (['A1.L1', 'A2.L1', 'A3.L2', 'A4.L2', 'A5.L2'] +
            ['A{}.L3'.format(value) for value in range(6,27)]) # list of all available stations

TOOLBAR_NAME = 'navigation_toolbar'
WINDOW_TITLE = 'Phase setpoint optimizer'

class MainWindow(QMainWindow):
    '''
        The main window of phase optimizer
    '''
    def __init__(self, initial_station):
        '''
            initialize MainWindow with an initial station
        '''
        super(MainWindow, self).__init__()
        uic.loadUi(MAIN_GUI_FILE, self) # load main gui in the class
        
        self.station = initial_station
        self.station_label.setText('Station: {}'.format(initial_station))
        # add graph
        self.phase_graph = PhaseGraph()
        self.graph_box.addWidget(self.phase_graph)

        # add navigation toolbar
        #self.navigation_toolbar = NavigationToolbar(self.phase_graph, self)
        #self.navigation_toolbar.setObjectName(TOOLBAR_NAME)
        #self.navigation_box.addWidget(self.navigation_toolbar)

        # button and combo boxes connections
        self.exit_button.clicked.connect(self.exit_button_clicked)
        self.optimize_button.clicked.connect(self.optimize_button_clicked)
        self.refresh_button.clicked.connect(self.refresh_button_clicked)
        self.tau_max.editingFinished.connect(self.tau_max_value_changed)
        self.tau_min.editingFinished.connect(self.tau_min_value_changed)
        self.amp_max.editingFinished.connect(self.amp_max_changed)
        self.amp_min.editingFinished.connect(self.amp_min_changed)

        # connect signals of optimization range checkboxes
        self.opt_start_time.editingFinished.connect(self.opt_start_time_changed)
        self.opt_stop_time.editingFinished.connect(self.opt_stop_time_changed)

        self.opt_start_time.valueChanged.connect(self.refresh_graph)
        self.opt_stop_time.valueChanged.connect(self.refresh_graph)

        # set title
        self.setWindowTitle(WINDOW_TITLE)

        # read data from doocs 
        self.initialize_station_data()
       
        # set maximum and minimum for the start point of optimization
        self.opt_start_time.setMaximum(self.knee * 1E6)
        self.opt_start_time.setMinimum(self.delay * 1E6)

        # set maximum and minimum for the stop point of optimization
        self.opt_stop_time.setMaximum(self.knee * 1E6)
        self.opt_stop_time.setMinimum(self.delay * 1E6)
        
        # set the default values 
        self.opt_start_time.setValue(self.delay * 1E6 + START_TIME_MARGIN)
        self.opt_stop_time.setValue(self.knee * 1E6 - STOP_TIME_MARGIN )

        # reset graph
        self.refresh_graph()

    @pyqtSlot()
    def exit_button_clicked(self):
        '''
            makes the program to stop
        '''
        self.close()

    @pyqtSlot()
    def refresh_button_clicked(self):
        '''
            makes the graph refreshing
        '''
        self.refresh_data()
        self.refresh_graph()

    @pyqtSlot()
    def optimize_button_clicked(self):
        '''
            starts the optimization steps
        '''
        self.refresh_button_clicked()

        if self.doocs_io.is_station_settings_corrects() == False:
            if SettingsDialog(self.station).exec_() == QDialog.Accepted:
                self.doocs_io.corrects_station_settings()
                time.sleep(TIME_DELAY)

        if self.doocs_io.is_station_settings_corrects() == True:
            optim = Optimizer(self.doocs_io,
                                self.amp_max.value(),
                                self.amp_min.value(),
                                self.tau_max.value()/1.0E6,
                                self.tau_min.value()/1.0E6,
                                self.opt_start_time.value()/1.0E6,
                                self.opt_stop_time.value()/1.0E6,
                                self.optimization_steps.value(),
                                self.avg_meas.value())
            
            progress_dialog = ProgressDialog()

            #connect the optimization progress signal
            optim.optimization_done.connect(progress_dialog.close)
            optim.pass_done.connect(progress_dialog.progress_bar.setValue)
            
            #start asyncronously
            optim.optimize_async()
            #start progressbar
            progress_dialog.exec_()
            # wait until thread completition
            optim.wait()

            if optim.succeded: 
                result_dialog = ResultDialog(self.station, optim.amp_old, 
                                             optim.amp_new, optim.tau_old, 
                                             optim.tau_new, optim.trace_old,
                                             optim.trace_new,
                                             optim.phase_corr_old, optim.phase_corr_new,
                                             self.delay, self.knee,
                                             self.phase)
                result = result_dialog.exec_()
                # if the value are accepted then are set in the station            
                if result == QDialog.Accepted:
                    self.doocs_io.set_amp_tau(optim.amp_new, optim.tau_new)
                    self.doocs_io.set_phase_corr(optim.phase_corr_new)
            else:
                error_dialog = ErrorDialog(self.station, optim.error_message) 
                result = error_dialog.exec_()
                result = False

            self.refresh_button_clicked()
            self.refresh_button_clicked()
            

    @pyqtSlot()
    def tau_max_value_changed(self):
        '''
            this method is called when tau_max_value is changed
        '''
        if self.tau_max.value() <= self.tau_min.value():
            self.tau_max.setValue(self.tau_min.value() + 1)
        # if max value < min set the two values equal

    @pyqtSlot()
    def tau_min_value_changed(self):
        '''
            this method is called when tau_min_value is changed
        '''
        if self.tau_min.value() >= self.tau_max.value():
            self.tau_min.setValue(self.tau_max.value() - 1)

        # if min value > min set the two values equal

    @pyqtSlot()
    def amp_max_changed(self,value):
        '''
            this method is called when tau_max_value is changed
        '''
        if self.amp_max.value() <= self.amp_min.value():
            self.amp_max.setValue(self.amp_min.value() + 1)

        # if max value < min set the two values equal
    
    @pyqtSlot()
    def amp_min_changed(self):
        '''
            this method is called when tau_min_value is changed
        '''
        if self.amp_min.value() >= self.amp_max.value():
            self.amp_min.setValue(self.amp_max.value() - 1)

        # if min value > min set the two values equal
   
    @pyqtSlot()
    def opt_start_time_changed(self):
        '''
            this method is called when the start optimization time is changed
        '''
        if self.opt_start_time.value() >= self.opt_stop_time.value():
            self.opt_start_time.setValue(self.opt_stop_time.value() - 1)

        self.refresh_graph()
  
    @pyqtSlot()
    def opt_stop_time_changed(self):
        '''
            this method is called when the start optimization time is changed
        '''
        if self.opt_stop_time.value() <= self.opt_start_time.value():
            self.opt_stop_time.setValue(self.opt_start_time.value() + 1)

        self.refresh_graph()


    @pyqtSlot(int)
    def initialize_station_data(self,index=None):
        '''
            this method is called when station changes:
        '''
        self.doocs_io = DoocsIO(self.station)
        self.refresh_data()
        self.refresh_graph()
        
    @pyqtSlot()
    def refresh_graph(self):
        '''
            refresh the graph
        '''
        self.phase_graph.clear()
        self.phase_graph.add_phase_graph(self.trace, 'Measured trace')
        self.phase_graph.add_setpoint_graph(self.amp_old, 
                                            self.tau_old,
                                            self.delay,
                                            self.knee,
                                            self.phase,
                                            'Setpoint')

        self.phase_graph.draw_line(float(self.opt_start_time.value())/1.0E6)
        self.phase_graph.draw_line(float(self.opt_stop_time.value())/1.0E6)
        self.phase_graph.display_graphs()

    def refresh_data(self):
        '''
            refresh read data
        '''
        # new handle
        self.trace = self.doocs_io.get_trace()
        (self.amp_old, self.tau_old) = self.doocs_io.get_amp_tau()
        (self.delay, self.knee, self.phase) = self.doocs_io.get_delay_knee_phase()
