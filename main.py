#!/usr/bin/env python3
#
# main file to be called to start phase optimizer.
# an optional parameter that define the initial station to be used can be passed
# in the format AX.LY where X is the station number (for XFEL X = 1..26)
# and Y is the accelerator (for XFEL Y = 1,2,3)
# otherwise a default station will be used
#

import sys
import os
from PyQt5.QtWidgets import QApplication
import main_gui
from main_gui import MainWindow, STATIONS

# Change the directory to the program one


EXECUTION_DIRECTORY = '/home/xfeloper/released_software/python/PhaseOptimizer'


if ((len(sys.argv) == 3) and 
    (sys.argv[1] in ['release', 'local']) and 
    (sys.argv[2] in STATIONS)): 
    # if the program is called by the jddd gui it has to change his current directory
    if sys.argv[1] == 'release':
        os.chdir(EXECUTION_DIRECTORY)
    # if exist a passed parameter that is member of STATIONS use it the station
    # to optimize
    station = sys.argv[2]
    app = QApplication(sys.argv)
    main_window = MainWindow(station) # create main_window object and display it
    main_window.show()
    sys.exit(app.exec_())
else:
    print('python main.py [release,local] station_id')
    print('station_ids:', STATIONS)
    exit(-1)

