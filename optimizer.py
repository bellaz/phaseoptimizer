#
# In this module there is an optimizer class to fit an exponential trace on
# the measured signal. it emits an error if the parameters go out of boundary
# The object is created passing the limits, mean number and 
#number of iterations to be done
# An 'execute' function performs the optimizations
# A signal is called each time a new tau-amp is found

from PyQt5.QtCore import QThread, pyqtSignal

# libraries to make the fit
import copy
import numpy as np
import scipy
from scipy.optimize import curve_fit
from math import exp

TIME_DELAY = 0.1

class Optimizer(QThread):
    '''
        a class that represents the object that makes exponential fits.
        the optimization can be called asyncronously
    '''

    optimization_done = pyqtSignal() # called when the optimization is finished
    pass_done = pyqtSignal(float) #called each time a tau/amp is discovered. It passes the percentage (0.0-100.0) of completition 
    def __init__(self, doocs_io, amp_max, amp_min, tau_max, tau_min, opt_start,
                 opt_stop, steps, avg_meas, parent = None):
        '''
            initializes the class with limits and parameters
            (ms is used as time magnitude)
        '''
        super(Optimizer, self).__init__(parent)

        # updated each optimization step
        self.doocs_io = doocs_io
        self.amp_max = amp_max
        self.amp_min = amp_min
        self.tau_max = tau_max
        self.tau_min = tau_min
        self.steps = steps
        self.avg_meas = avg_meas
        self.opt_start = opt_start # when optimization starts
        self.opt_stop = opt_stop # when optimization stops

    def run(self):
        '''
            For QThread specification
        '''
        self.optimize()

    def optimize_async(self):
        '''
            Start async optimization
        '''
        self.start()

    def optimize(self):
        '''
            perform the optimization steps
            if something fails or go out of boundaries it throws an error.
        '''
        
        self.completed_steps = 0
         # set initial error message
        self.error_message = 'Ok'
        # set initial succeded value
        self.succeded = False

        self.knee = 0.0
        self.delay = 0.0
        self.phase = 0.0
      
        self.amp_new = 0.0
        self.tau_new = 0.0
        self.amp_old = 0.0
        self.tau_old = 0.0
        self.phase_corr_old = 0.0
        self.phase_corr_new = 0.0

        try:
            # read old amp-tau
            (self.amp_old, self.tau_old) = self.doocs_io.get_amp_tau()
            # copy to 'new' values
            (self.amp_new, self.tau_new) = (self.amp_old, self.tau_old)
            # get phase correction
            self.phase_corr_old = self.doocs_io.get_phase_corr()
            self.phase_corr_new = self.phase_corr_old
        except:
            self.error_message = 'Failed to get old amp/tau values'
            self.trace_new = [[],[]]
            self.optimization_done.emit()
            self.disconnect_signals()
            return

        try:
            # read self.delay self.knee and self.phase
            (self.delay, self.knee, self.phase) = self.doocs_io.get_delay_knee_phase()
            # old trace
            self.trace_old = self.doocs_io.get_trace()
        except:
            self.crash('Error reading initial data')
            return             

        for i in range(self.steps):
        # fit function
            try:
                (time, trace) = self.get_trace_to_fit()
            
            except: # must add doocs_error codes
                self.crash('Unable to read trace data')
                return

            def setpoint(t, amp, tau, phase):
                '''
                    first parameter is the independent variable time
                    amp and tau are the parameters to fit
                '''
            
                base_trace = (self.phase + phase +
                             amp*(np.exp(-t/tau)-exp(-self.knee/tau))/
                             (exp(-self.knee/tau) - exp(-self.delay/tau)))

                return base_trace
            
            try:
                # get data
                fit_data, _ = curve_fit(f = setpoint,
                                        xdata = list(time),
                                        ydata = list(trace),
                                        p0 = [self.amp_new, self.tau_new, self.phase_corr_new])
            except ValueError: # must add doocs_error codes
                self.crash('Bad phase data')
                return
            except RuntimeError:
                self.crash('Unable to fit')
                return
            except scipy.optimize.OptimizeWarning:
                ('opt warning')

            # discretize the resulting values in degree-us
            fit_data[0] = round(fit_data[0])
            fit_data[1] = 1.0E-6 * round(fit_data[1] * 1.0E6)
            
            # set new data
            self.amp_new = fit_data[0]
            self.tau_new = fit_data[1]
            self.phase_corr_new -= fit_data[2]
            # check if the values are in the limits
            if ((self.amp_new > self.amp_max) or 
                (self.amp_new < self.amp_min)):
                    self.crash('Amplitude out of boundaries')
                    return

            if ((self.tau_new > self.tau_max) or
                (self.tau_new < self.tau_min)):
                    self.crash('Tau out of boundaries')
                    return
            
            # set new values in the station
            try:
                self.doocs_io.set_amp_tau(self.amp_new, self.tau_new)
                self.doocs_io.set_phase_corr(self.phase_corr_new)
            except:
                self.crash('Unable to set amp tau values')
                return

            # emit an update if successful
            self.pass_done.emit(100.0*float(i+1)/self.steps)
            self.completed_steps = i+1
        
        # get the newly optimized trace and return emitting a done signal
        try:
            self.trace_new = self.doocs_io.get_trace()
        except:
            self.crash('Unable to get new trace')
            return
        
        self.return_initial_status()
        self.succeded = True
        self.optimization_done.emit()
        self.disconnect_signals()
    
    def get_trace_to_fit(self):
        '''
            get the trace to fit smoothed and cut (removed initial optimization
            start until knee
        '''
        # get a trace
        (time, trace_acc) = self.doocs_io.get_trace()

        for i in range(self.avg_meas - 1):
            trace_acc += self.doocs_io.get_trace()[1]
        
        trace = (time, trace_acc)


        filtered_trace = filter(lambda data: ((data[0] >= self.opt_start) and
                                              (data[0] < self.opt_stop)),
                                              zip(*trace))
        
        
        (time, trace_acc) = tuple(zip(*filtered_trace))
       
        time = np.array(time)
        trace_acc = np.array(trace_acc)
        # return a mean value data
        return (time, trace_acc/self.avg_meas)
    
  
    def crash(self, message):
        '''
            set error message and return 
        '''
        self.error_message = message
        self.return_initial_status()
        self.trace_new = [[],[]]
        self.optimization_done.emit()
        self.disconnect_signals()
        
    def return_initial_status(self):
        '''
            return to initial status
        '''
        self.doocs_io.set_amp_tau(self.amp_old, self.tau_old)
        self.doocs_io.set_phase_corr(self.phase_corr_old)
        

    def disconnect_signals(self):
        '''
            disconnect all signals
        '''
        self.pass_done.disconnect()
        self.optimization_done.disconnect()
