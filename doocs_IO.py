#
# Provide syntactic sugar access to doocs properties of a module needed for
# phase optimizations
# the class accept a station that can't be changed (for security)
# properties available:
# phase trace data
# amp and tau setpoint values 
# LFF status
# is possible to reset LFF, set its status. It's possible to set amp and tau.
# a function to reset the tau-amp value to initial status is provided.

SP_PHASE_ADDR = 'XFEL.RF/LLRF.CONTROLLER/CTRL.{}/SP.PHASE'
START_TIME_ADDR = 'XFEL.RF/LLRF.CONTROLLER/CTRL.{}/PHASEMODULATION.SP_START_TIME'
STOP_TIME_ADDR = 'XFEL.RF/LLRF.CONTROLLER/CTRL.{}/PHASEMODULATION.SP_STOP_TIME'
MAX_PHASE_OFFSET_ADDR = 'XFEL.RF/LLRF.CONTROLLER/CTRL.{}/PHASEMODULATION.SP_MAX_PHASE_OFFSET'
TIME_CONSTANT_ADDR = 'XFEL.RF/LLRF.CONTROLLER/CTRL.{}/PHASEMODULATION.SP_TIME_CONSTANT'
PHASE_TD_ADDR = 'XFEL.RF/LLRF.CONTROLLER/VS.{}/PHASE.TD'
PULSE_DELAY_ADDR = 'XFEL.RF/LLRF.CONTROLLER/MAIN.M12.{}/PULSE_DELAY'
SP_EXT_DELAY_ADDR = 'XFEL.RF/LLRF.CONTROLLER/CTRL.{}/SP.EXT_DELAY'

FF_ENA_ADDR = 'XFEL.RF/LLRF.CONTROLLER/CTRL.{}/FF.ENA'
OVC_ENA_ADDR = 'XFEL.RF/LLRF.CONTROLLER/OVC.{}/ENA'
FB_ENA_ADDR = 'XFEL.RF/LLRF.CONTROLLER/CTRL.{}/FB.ENA'
FFC_ENA_ADDR = 'XFEL.RF/LLRF.CONTROLLER/CTRL.{}/FFC.ENA'
LFF_ENA_ADDR = 'XFEL.RF/LLRF.CONTROLLER/LFF.{}/LFF.ENA'
BLC_ENA_ADDR = 'XFEL.RF/LLRF.CONTROLLER/BLC.{}/TABLE.ENA'


LFF_RESET_ADDR = 'XFEL.RF/LLRF.CONTROLLER/LFF.{}/RESET'


OVC_ROTATION_ADDR = 'XFEL.RF/LLRF.CONTROLLER/CTRL.{}/ANGLE.OUT_ROTATION'


SLEEP_BETWEEN_MEASURES = 0.02 #seconds to sleep between each trace request

import pydoocs

# for converting trace data to numpy.array
import numpy as np

# for sleep
import time as time_

class DoocsIO(object):
    '''
        class that provides easy access of doocs interface
    '''

    def __init__(self, station):
        '''
            initializes the object with a station string identifier (ex. 'A13').
        '''
        self.station = station
        self.pulse_id = None # to distinguish different pulses

    def get_amp_tau(self):
        '''
            read amp and tau. It returns (amp, tau) 
        '''
        amp = pydoocs.read(MAX_PHASE_OFFSET_ADDR.format(self.station))['data'] # this is negative
        tau = pydoocs.read(TIME_CONSTANT_ADDR.format(self.station))['data'] * 1.0E-6 # this is positive and normalized to seconds
        return(amp, tau)

    def set_amp_tau(self, amp, tau):
        '''
            write amp and tau 
        '''
        pydoocs.write(MAX_PHASE_OFFSET_ADDR.format(self.station), int(amp))
        pydoocs.write(TIME_CONSTANT_ADDR.format(self.station), int(tau * 1.0E6))

    def get_trace(self):
        '''
            get trace data as (time, trace)
        '''
        read_pulse_id = self.pulse_id

        read_data = None

        #cycle until an unique pulse is read
        while(read_pulse_id == self.pulse_id):
            
            read_data = pydoocs.read(PHASE_TD_ADDR.format(self.station))
            read_pulse_id = read_data['timestamp']
            time_.sleep(SLEEP_BETWEEN_MEASURES)

        self.pulse_id = read_pulse_id
        # if phase = 0 sample data is positive
        (time, trace) = tuple(zip(*read_data['data']))
	
        # seconds units
        time = [ t*1.0E-6 for t in time]

        return (np.array(time), np.array(trace))

    def get_delay_knee_phase(self):
        '''
            get delay knee as (delay, knee, phase)
        '''
        ext_delay = pydoocs.read(SP_EXT_DELAY_ADDR.format(self.station))['data']
        pulse_delay = pydoocs.read(PULSE_DELAY_ADDR.format(self.station))['data']
        start = pydoocs.read(START_TIME_ADDR.format(self.station))['data']
        stop = pydoocs.read(STOP_TIME_ADDR.format(self.station))['data']
        phase = pydoocs.read(SP_PHASE_ADDR.format(self.station))['data']
        
        delay = (ext_delay + pulse_delay + start) * 1.0E-6 # Add all the delays .. 
        knee = (ext_delay + pulse_delay + stop) * 1.0E-6 # and normalize to seconds

        return(delay, knee, phase)

    def get_LFF_status(self):
        '''
            get LFF status True -> enabled, False -> disabled
        '''
        lff_status = pydoocs.read(FFC_ENA_ADDR.format(self.station))['data']
        return bool(lff_status)

    def set_LFF_status(self, value):
        '''
            set LFF status True -> enabled, False -> disabled
        '''
        pydoocs.write(FFC_ENA_ADDR.format(self.station), value)

    def reset_LFF(self):
        '''
            reset LFF tables
        '''
        pydoocs.write(LFF_RESET_ADDR.format(self.station), 1)


    def get_phase_corr(self):                                                       
        '''                                                                         
            get phase correction                                                
        '''                                                                     
        return pydoocs.read(OVC_ROTATION_ADDR.format(self.station))['data']
                                                                                 
    def set_phase_corr(self, value):                                            
        '''                                                                     
            set phase correction                                                
        '''                                                                      
        if value > 180.0:
                value = value - 360.0

        if value < -180.0:
                value = value + 360.0

        pydoocs.write(OVC_ROTATION_ADDR.format(self.station), value)

    def is_station_settings_corrects(self):
        '''
            finds if all station settings are correct
        '''
        result =    ((pydoocs.read(FF_ENA_ADDR.format(self.station))['data'] == True) and
                    (pydoocs.read(OVC_ENA_ADDR.format(self.station))['data'] == False) and
                    (pydoocs.read(FB_ENA_ADDR.format(self.station))['data'] == False) and
                    (pydoocs.read(FFC_ENA_ADDR.format(self.station))['data'] == False) and
                    (pydoocs.read(LFF_ENA_ADDR.format(self.station))['data'] == False) and
                    (pydoocs.read(BLC_ENA_ADDR.format(self.station))['data'] == False))
        
        return result

    def corrects_station_settings(self):
        '''
            applies correct settings
        '''
        pydoocs.write(BLC_ENA_ADDR.format(self.station), False)
        pydoocs.write(LFF_ENA_ADDR.format(self.station), False)
        pydoocs.write(FFC_ENA_ADDR.format(self.station), False)
        pydoocs.write(FB_ENA_ADDR.format(self.station), False)
        pydoocs.write(OVC_ENA_ADDR.format(self.station), False)
        pydoocs.write(FF_ENA_ADDR.format(self.station), True)
