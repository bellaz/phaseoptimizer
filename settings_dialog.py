
#
# This module provides a dialog window when the station settings have to be
# changed
#

from PyQt5 import QtCore, uic
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog

# ui file
SETTINGS_GUI_FILE = 'set_flags.ui'
#cavity label
SETTINGS_TEXT = 'The following settings are going to be applied on {}:'

class SettingsDialog(QDialog):
    '''
        This class provide a popup that shows up when trying to optimize a cavity
        with incorrect settings
    '''
    def __init__(self, station, parent = None):
        '''
            the affected station and must be provided
        '''
        
        super(SettingsDialog, self).__init__(parent)
        uic.loadUi(SETTINGS_GUI_FILE, self)

        # set cavity label
        self.station_label.setText(SETTINGS_TEXT.format(station))
