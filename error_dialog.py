
#
# This module provides a dialog window when the optimization failed 
#
# In this Popup the station affected and the reason of 
# optimization failure is presented
#

from PyQt5 import QtCore, uic
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog

ERROR_GUI_FILE = 'error_gui.ui'

class ErrorDialog(QDialog):
    '''
        this class provide the error dialog
    '''
    def __init__(self, station, error, parent = None):
        '''
            the affected station and the error must be provided
        '''
        
        super(ErrorDialog, self).__init__(parent)
        uic.loadUi(ERROR_GUI_FILE, self)

        # set errors and labels
        self.setWindowTitle('Optimization of ' + station + ' failed')
        self.error_label.setText(error)
        self.buttonBox.accepted.connect(self.close)
