
#
# This module provides a dialog window that presents a successful optimization of
# the phase setpoint. 
#
# In the graph the previous and proposed setpoint is shown with their measured
# phase curves (4 graphs)
# The numerical variations of tau and initial phase is shown in a grid
#

from PyQt5 import QtGui, QtCore, uic
from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import QDialog

# to plot the phase graph
from phase_graph import PhaseGraph 

from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

RESULT_GUI_FILE = 'result_gui.ui'

TOOLBAR_NAME = 'navigation_toolbar'

class ResultDialog(QDialog):
    '''
        this class provide the result dialog
    '''
    def __init__(self, station, amp_old, amp_new, tau_old, tau_new, old_trace, 
            new_trace, phase_corr_old, phase_corr_new, delay, knee, phase, parent = None):
        '''
            It shows the result of optimization and lets the user to choose
        '''
        super(ResultDialog, self).__init__(parent)
        uic.loadUi(RESULT_GUI_FILE, self)

        # add graph
        phase_graph = PhaseGraph()
        self.graph_box.addWidget(phase_graph)

        # clear graph and print the old and new traces
        phase_graph.clear()
        phase_graph.add_phase_graph(old_trace, 'Old trace')
        phase_graph.add_setpoint_graph(amp_old, tau_old, 
                                       delay, knee, phase, 'Old setpoint')
        phase_graph.add_phase_graph(new_trace, 'New trace')
        phase_graph.add_setpoint_graph(amp_new, tau_new,
                                       delay, knee, phase, 'New setpoint') 
        phase_graph.display_graphs()

        # add navigation toolbar
        #self.navigation_toolbar = NavigationToolbar(phase_graph, self)
        #self.navigation_toolbar.setObjectName(TOOLBAR_NAME)
        #self.navigation_box.addWidget(self.navigation_toolbar)

        # set labels text
        self.setWindowTitle('Fit result on ' + station + ' successful:')
        self.accept_label.setText('Apply setpoint parameters to ' + station + ':') 

        # set values
        self.amp_old_el.setText('{}'.format(int(amp_old)))
        self.amp_new_el.setText('{}'.format(int(amp_new)))
        self.tau_old_el.setText('{}'.format(int(tau_old*1.0E6)))
        self.tau_new_el.setText('{}'.format(int(tau_new*1.0E6)))
        self.phase_corr_old_el.setText('{0:.3}'.format(phase_corr_old))
        self.phase_corr_new_el.setText('{0:.3}'.format(phase_corr_new))
