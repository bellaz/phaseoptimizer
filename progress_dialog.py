#
# This module provide a progress bar to show the status of optimization
# It create and communicate with the optimizer

from PyQt5 import QtGui, QtCore, uic
from PyQt5.QtWidgets import QDialog

PROGRESS_GUI_FILE = 'progress_gui.ui'
WINDOW_TITLE = 'Optimizing ..'

class ProgressDialog(QDialog):
    '''
        Dialog that shows a progress bar
    '''
    def __init__(self, parent = None):
        '''
            Initialize the dialog
        '''
        super(ProgressDialog, self).__init__(parent, 
                                             (QtCore.Qt.CustomizeWindowHint |
                                             QtCore.Qt.WindowTitleHint))
        uic.loadUi(PROGRESS_GUI_FILE, self)
        
        self.setWindowTitle(WINDOW_TITLE)
