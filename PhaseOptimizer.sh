# script to start the phase optimization program
# anaconda4 python as default python interpreter
export PATH=/opt/anaconda4/bin:$PATH

# path settings for using python in HLC
if [ `uname -s` = "Darwin" ] ; then
	# pydoocs and PyTine
	export PYTHONPATH=/local/lib:/local/tine/lib
elif [ `uname -s` = "Linux" ] ; then
	# pydoocs and PyTine
	export PYTHONPATH=/local/lib:/usr/share/libtine/python/python3.6m
fi

exec python3 /home/xfeloper/released_software/python/PhaseOptimizer/main.py $1 $2

